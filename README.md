# Puente H: control de motores

<img src="https://www.build-electronic-circuits.com/wp-content/uploads/2018/11/H-bridge.png" alt="fig. 1" width="300"/>

<img src="https://sktechworks.ca/wp-content/uploads/2020/08/L298-2-2046x2048.jpg" alt="fig. 2" width="300"/>

## Descripción

Este módulo contiene un [circuito integrado L298N](https://www.st.com/en/motor-drivers/l298.html) que permite controlar 2 [motores DC](https://vid.puffyan.us/watch?v=LAtPHANEfQo) o 1 [motor paso a paso](https://vid.puffyan.us/watch?v=eyqwLiowZiU).

## Especificaciones

El módulo tiene 7 terminales (pines) de conexión:
- 12V: fuente de alimentación.
- GND: conexión a masa. Conectar a GND (ground)
- 5V: salida de 5V.
- out1 y out3: conexiones de puente H 1.
- out3 y out4: conexiones de puente H 2.

## Diagrama de conexión con Arduino

<img src="img/Schematic.webp" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software).

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.

### AHORA VEAMOS LOS MOTORES FUNCIONAR!!!
